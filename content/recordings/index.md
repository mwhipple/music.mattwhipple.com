---
menu: main
title: Recordings
weight: 4
featured_image: '/images/hero_HPF.jpg'
---

At some point in the next couple years I'll probably work on getting
some more intentional recordings together (and just try to get better
at recording in general) but in the meantime here are some fairly raw
or discovered recordings.

---

### "Unanswered" (original tempo) at Union Brew House

(not sure where "Honey, I'm Here" came from)

{{< youtube IEhfnKo6KGQ >}}

### "Balladeer" at Union Brew House

This is a song written by my father, Sherman Whipple.

{{< youtube os77fzMJD34 >}}

### Fortune Panda Practice Reel

My old band "Fortune Panda" got together for some practices recently,
I'll be incrementally putting together a sample of the songs we went
through. This will be added to over time and I'm working backwards
through the recordings where our drummer Dave missed couldn't make the
last couple.

[Fortune Panda Practices](https://storage.googleapis.com/mw-recordings/FP_Mix1.mp3)