---
menu: main
title: About
weight: 5
featured_image: '/images/hero_HPF.jpg'
---

![Performing at Old Ship](old-ship-bw.jpg)

I'm gradually returning to performing after a fairly long hiatus
to focus on my family and career.

I have fairly diverse musical tastes and am currently
looking to expand the breadth and depth of my knowledge and skills.
