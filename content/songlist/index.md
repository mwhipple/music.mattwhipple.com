---
menu: main
title: Songlist
type: songlist
weight: 3
featured_image: '/images/hero_HPF.jpg'
---

This is as much for my own sake as for others but will have other uses as it grows.

This is a songlist that will be gradually built up. I had actually planned on paring
this list way down but immediately after that I somewhat ironically got a gig; so
instead I'll add to this to include songs as I prepare for different shows, starting
with a fresh list for that show.
