HUGO := hugo

run-local:
	$(HUGO) server -D
.PHONY: run-local

site:
	$(HUGO)
.PHONY: site

deploy: site
	$(HUGO) deploy
.PHONY: deploy
